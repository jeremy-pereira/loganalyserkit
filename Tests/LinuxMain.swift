import XCTest

import LogAnalyserKitTests

var tests = [XCTestCaseEntry]()
tests += LogAnalyserKitTests.allTests()
XCTMain(tests)
