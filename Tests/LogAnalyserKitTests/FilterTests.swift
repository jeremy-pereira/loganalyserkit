//
//  FilterTests.swift
//
//  Created by Jeremy Pereira on 27/12/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Foundation
import Toolbox
@testable import LogAnalyserKit

private let log = Logger.getLogger("LogAnalyserTests.FilterTests")

final class FilterTests: XCTestCase
{
	func testSimpleFilter()
	{
		let input = """
		line1
		#line2
		line3
		"""
		let data = Data(input.utf8)
		var filter = EventSplitter(AnySource([data])).filter
		{
			!$0.raw.hasPrefix("#")
		}
		do
		{
			guard let line1 = try filter.next()
			else
			{
				XCTFail("Line 1 is missing from the events")
				return
			}
			XCTAssertTrue(checkString(key: "_raw", event: line1, expected: "line1"))
			XCTAssert(line1.raw == "line1", "Expected line1, got \(line1.raw)")
			guard let line2 = try filter.next()
			else
			{
				XCTFail("Line 2 is missing from the events")
				return
			}
			XCTAssertTrue(checkString(key: "_raw", event: line2, expected: "line3"), "Got \(line2.raw)")
			if let line3 = try filter.next()
			{
				XCTFail("Line 3 should be missing from the events: \(line3.raw)")
				return
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	// TODO: Factor out as this is copied from EventSplitterTests
	private func checkString(key: String, event: Event, expected: String) -> Bool
	{
		guard let rawValue = event.value(forKey: Event.rawKey)
		else
		{
			log.error("event[\(Event.rawKey)] is missing")
			return false
		}
		switch rawValue
		{
		case .text(let string):
			guard string == expected
			else
			{
				log.error("Invalid raw: '\(string)', expected '\(expected)'")
				return false
			}
		default:
			log.error("Invalid value \(rawValue)")
			return false
		}
		return true
	}


	static var allTests =
	[
		("testSimpleFilter", testSimpleFilter),
	]
}
