//
//  FieldExtractTests.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2020.
//

import XCTest
import Foundation
import Toolbox
@testable import LogAnalyserKit

private let log = Logger.getLogger("LogAnalyserTests.FilterTests")

final class FieldExtractTests: XCTestCase
{
	func testSimpleFieldExtract()
	{
		let input = """
		1x 2x 3x 4x 5x 6x
		"""
		let data = Data(input.utf8)
		var extractor = FieldExtract(EventSplitter(AnySource([data])),
									 regEx: try! NSRegularExpression(pattern: #"(\d)x (\d)x (\dx)"#,
																	 options: []),
									 fieldNames: ["a", "b", "c"])
		do
		{
			guard let line1 = try extractor.next()
			else
			{
				XCTFail("Line 1 is missing from the events")
				return
			}
			if let a = line1.value(forKey: "a")
			{
				XCTAssert(a == Value.text("1"), "field a is wrong: \(a)")
			}
			else
			{
				XCTFail("Failed to capture a")
			}
			if let value = line1.value(forKey: "b")
			{
				XCTAssert(value == Value.text("2"), "field b is wrong: \(value)")
			}
			else
			{
				XCTFail("Failed to capture b")
			}
			if let value = line1.value(forKey: "c")
			{
				XCTAssert(value == Value.text("3x"), "field c is wrong: '\(value)'")
			}
			else
			{
				XCTFail("Failed to capture c")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testNoCaptures()
	{
		let input = """
		1x 2x 3x 4x 5x 6x
		"""
		let data = Data(input.utf8)
		var extractor = FieldExtract(EventSplitter(AnySource([data])),
									 regEx: try! NSRegularExpression(pattern: #"\dx \dx \dx"#,
																	 options: []),
									 fieldNames: ["a", "b", "c"])
		do
		{
			guard let line1 = try extractor.next()
			else
			{
				XCTFail("Line 1 is missing from the events")
				return
			}
			if let a = line1.value(forKey: "a")
			{
				XCTFail("field a should not exist \(a)")
			}
			if let b = line1.value(forKey: "b")
			{
				XCTFail("field b should not exist \(b)")
			}
			if let c = line1.value(forKey: "c")
			{
				XCTFail("field c should not exist \(c)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	static var allTests =
	[
		("testSimpleFieldExtract", testSimpleFieldExtract),
		("testNoCaptures", testNoCaptures),
	]
}
