//
//  TimeExtractTests.swift
//
//  Created by Jeremy Pereira on 27/12/2020.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Foundation
import Toolbox
@testable import LogAnalyserKit

private let log = Logger.getLogger("LogAnalyserTests.TimeExtractTests")

final class TimeExtractTests: XCTestCase
{
	func testSimpleExtract()
	{
		let timeOfTest = Date()	// If we get a time after this, we didn't extract properly

		let input = """
		2020-05-15 07:02:54 line1
		2020-05-15 07:02:55 #line2
		2020-05-16 07:02:54 line3
		"""
		let data = Data(input.utf8)
		let dateFormat = DateFormatter()
		dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
		let context = try! NSRegularExpression(pattern: #"\d*-\d*-\d* \d*:\d*:\d*"#, options: [])
		var extractor = EventSplitter(AnySource([data])).timeExtract(format: dateFormat, context: context)
		do
		{
			guard let line1 = try extractor.next()
			else
			{
				XCTFail("Line 1 is missing from the events")
				return
			}
			if let times = line1.value(forKey: Event.timeKey)
			{
				switch times
				{
				case .date(let time):
					XCTAssertTrue(time < timeOfTest, "Wrong time: \(time)")
				default:
					XCTFail("_time is not a time value")
				}
			}
			else
			{
				XCTFail("Got no time")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	// TODO: Factor out as this is copied from EventSplitterTests
	private func checkString(key: String, event: Event, expected: String) -> Bool
	{

		guard let rawValues = event.value(forKey: Event.rawKey)
		else
		{
			log.error("event[\(Event.rawKey)] count should be 1")
			return false
		}
		switch rawValues
		{
		case .text(let string):
			guard string == expected
			else
			{
				log.error("Invalid raw: '\(string)', expected '\(expected)'")
				return false
			}
		default:
			log.error("Invalid value \(rawValues)")
			return false
		}
		return true
	}


	static var allTests =
	[
		("testSimpleExtract", testSimpleExtract),
	]
}
