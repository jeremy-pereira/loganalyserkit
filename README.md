# LogAnalyserKit

## Introduction
This package provides a toolkit for analysing log files such as, for example, Apache logs or other server logs. It's heavily influenced by [Splunk](https://www.splunk.com) but provides no real time functionality. 

## Installation
This is a Swift package. Add it to your application as a dependency as per any normal package and import `LogAnalyserKit`. 

## Design

### Entities

#### Event
Events are the basic unit of a log. Events are dictionaries where the values are strings or numbers or time stamps or lists of the same. Keys are structured so we an have name spaces. `.` is used as a separator.

There are some keys which all events have

- `timestamp` parsed from the event
- `sequenceNumber` assigned as the event is parsed (monotonically increasing
- `raw` the unprocessed tex tof the event
- `source` by default the name of the file from which it came
- `sourceType` the type of event e.g. access log etc.

The sourceType tells us most things about how to parse events and process them subsequently.

We should have a mechanism for updating events asynchronously.

### Processing

Initially when we take in a file we need to split it into events.



