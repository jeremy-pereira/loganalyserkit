// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LogAnalyserKit",
	platforms: [
		.macOS(.v10_15),
	],

    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "LogAnalyserKit",
            targets: ["LogAnalyserKit"]),
		.executable(name: "log-analyser", targets: ["log-analyser"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
		.package(url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/Toolbox.git", from: "20.0.3"),
		.package(name: "Statistics", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/statistics.git", from: "3.1.0"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "0.3.0"),
   ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "LogAnalyserKit",
            dependencies: [
				.product(name: "Toolbox", package: "Toolbox"),
				.product(name: "Statistics", package: "Statistics"),
			]),
        .testTarget(
            name: "LogAnalyserKitTests",
            dependencies: ["LogAnalyserKit",
						   .product(name: "Toolbox", package: "Toolbox")]),
		.target(
			name: "log-analyser",
			dependencies: ["LogAnalyserKit",
						   .product(name: "Toolbox", package: "Toolbox"),
						   .product(name: "Statistics", package: "Statistics"),
						   .product(name: "ArgumentParser", package: "swift-argument-parser")
			]),

    ]
)
