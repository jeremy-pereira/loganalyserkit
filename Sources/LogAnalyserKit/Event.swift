//
//  Event.swift
//  
//
//  Created by Jeremy Pereira on 27/12/2020.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// An event
///
/// Events are collections of field value pairs. They form the basis of everything
/// we do.
///
/// Events are conceptually
/// mutable but actual instances are not, which is to say that, once you have
/// an event, it won't change but you may receive a new event with the same uid
/// that conceptually replaces the current event.
import Foundation

public struct Event
{
	/// Key for the raw data
	public static let rawKey = "_raw"
	public static let timeKey = "_time"
	public static let errorKey = "_error"

	public init(string: String)
	{
		fields = [
			Event.rawKey : .text(string),
			Event.timeKey : .date(Date()),
		]
	}
	public init(error: String)
	{
		fields = [
			Event.errorKey : .text(error),
			Event.rawKey : .text(""),
			Event.timeKey : .date(Date())
		]
	}
	/// The underlying dictionary with the fields in it.
	public private(set) var fields: [String: Value]
	/// Get the values associated with a key
	///
	/// - Parameter key: The key for the values to get
	/// - Returns: The values associated with the event for the given key
	public func value(forKey key: String) -> Value?
	{
		return fields[key]
	}

	/// Set a value for a key
	///
	/// Any existing values disappear
	/// - Parameters:
	///   - key: The key to set
	///   - value: The new value
	public mutating func set(key: String, value: Value)
	{
		fields[key] = value
	}

	/// The raw data for this event
	public var raw: String
	{
		let value = fields["_raw"]!
		switch value
		{
		case .text(let aString):
			return aString
		default:
			fatalError("_raw hould be a text value")
		}
	}
	/// True if this is an error event
	public var isError: Bool { fields[Event.errorKey] == nil }
}
