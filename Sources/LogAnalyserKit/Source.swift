//
//  Source.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2020.
//
//  Copyright (c) Jeremy Pereira 2020
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// A source of events and other things
public protocol Source
{
	/// The type of thing it outputs
	associatedtype Out

	/// The type of thing it outputs
	///
	/// - Returns: The next thing or nil if we are at the end of the stream
	/// - Throws: If something goes wrong getting the next thing.
	mutating func next() throws -> Out?
}

/// A type erased wrapper for any source
public struct AnySource<Out>: Source
{
	let wrappedNext: () throws -> Out?

	init<S: Source>(_ input: S) where S.Out == Out
	{
		var mutableInput = input
		wrappedNext = { try mutableInput.next() }
	}

	init<S: Sequence>(_ input: S) where S.Element == Out
	{
		var iterator = input.makeIterator()
		wrappedNext = { iterator.next() }
	}

	public mutating func next() throws -> Out?
	{
		return try wrappedNext()
	}

	public static func wrap<S: Source>(_ wrappee: S) -> AnySource<S.Out>
	{
		if let anySource = wrappee as? AnySource<S.Out>
		{
			return anySource
		}
		else
		{
			return AnySource<S.Out>(wrappee)
		}
	}
}

