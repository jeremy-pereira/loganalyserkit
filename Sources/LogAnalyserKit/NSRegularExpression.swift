//
//  NSRegularExpression.swift
//
//  Created by Jeremy Pereira on 28/12/2020.
//
//  Copyright (c) Jeremy Pereira 2020
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

extension NSRegularExpression
{
	/// A convenience function to get the first matched string for a regular expression
	/// - Parameters:
	///   - string: The string to match on
	///   - options: Any regular expression matching options
	/// - Returns: The first matched string
	func firstMatchString(in string: String, options: NSRegularExpression.MatchingOptions = []) -> String?
	{
		let range = NSRange(location: 0, length: string.utf16.count)
		guard let result = self.firstMatch(in: string, options: options, range: range) else { return nil }

		let matchRange = result.range
		let nsString: NSString = NSString(string: string)
		let matchedSubString = nsString.substring(with: matchRange)
		return matchedSubString
	}
}
