//
//  EventSplitter.swift
//  
//
//  Created by Jeremy Pereira on 26/12/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// A source of raw event strings from `Data` objects
///
/// Event strings are created by parsing a sequence of Data objects
/// into lines of text. If an input data object is not terminated by the event
/// separator, the left over will be prpended to the beginning of the next
/// data object.
///
/// - Todo:
/// 	- Make the event separator configurable
/// 	- Make it possible to parse text that isn't UTF-8
public struct EventSplitter: Source
{
	private var source: AnySource<Data>
	private var currentData: Data?
	private var start: Int = 0
	private var primed = false

	public init<S: Source>(_ source: S)  where S.Out == Data
	{
		self.source = AnySource<Data>.wrap(source)
	}

	public mutating func next() throws -> Event?
	{
		if !primed
		{
			currentData = try source.next()
			primed = true
		}
		guard var theData = currentData else { return nil }
		var splitData: [Data] = theData.split(at: 0xa)
		while splitData.count < 2, let newData = try source.next()
		{
			theData += newData
			splitData = theData.split(at: 0x0a)
		}
		currentData = splitData.second
		guard let line =  String(bytes: splitData.first!, encoding: .utf8)
		else
		{
			return Event(error: "Failed to convert event bytes to UTF-8")
		}
		return Event(string: line)
	}
}

fileprivate extension Data
{
	func split(at byte: UInt8) -> [Data]
	{
		return self.split(separator: byte, maxSplits: 1, omittingEmptySubsequences: false)
	}
}

fileprivate extension Array
{
	var second: Element? { return count > 1 ? self[1] : nil }
}
