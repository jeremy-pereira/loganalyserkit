//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 26/12/2020.
//

import Foundation

public struct FileReader: Source
{
	private var fileNames: AnyIterator<URL>
	public init<S: Sequence>(_ input: S) where S.Element == URL
	{
		fileNames = AnyIterator(input.makeIterator())
	}

	public mutating func next() throws -> Data?
	{
		guard let nextFile = fileNames.next() else { return nil }
		let data = try Data(contentsOf: nextFile)
		return data
	}
}
