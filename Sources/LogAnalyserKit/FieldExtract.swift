//
//  FieldExtract.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2020.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// Field extract transform.
///
/// Give it a list of field names and a regular expression with capture groups
/// corresponding to the field names to extract fields into the event.
///
/// If the number of capture groups is less than the number of field names,
/// the remaining field names will be ignored.
public struct FieldExtract: Source
{
	private var source: AnySource<Event>
	private let sourceField: String
	private let regEx: NSRegularExpression
	private let fieldNames: [String]

	/// Create a field extract
	/// - Parameters:
	///   - source: The source of events on which to extract fields
	///   - sourceField: The field in the source on which to extract, defaults to `_raw`
	///   - regEx: The regular expression with a capture group for each field name
	///   - fieldNames: The names of the new fields
	public init<S: Source>(_ source: S,
						   sourceField: String = Event.rawKey,
						   regEx: NSRegularExpression,
						   fieldNames: [String])
	where S.Out == Event
	{
		self.source = AnySource<Event>.wrap(source)
		self.sourceField = sourceField
		self.regEx = regEx
		self.fieldNames = fieldNames
	}

	public mutating func next() throws -> Event?
	{
		guard var event = try source.next() else { return nil }
		if let fieldString = event.value(forKey: sourceField)
		{
			let nsString: NSString = NSString(string: fieldString.asString)
			let range = NSRange(location: 0, length: nsString.length)
			if let result = regEx.firstMatch(in: fieldString.asString, options: [], range: range),
			   result.numberOfRanges > 1
			{
				var fieldIterator = fieldNames.makeIterator()
				for i in 1 ..< result.numberOfRanges
				{
					guard let fieldName = fieldIterator.next() else { break }
					let range = result.range(at: i)
					if range.location != NSNotFound
					{
						let valueString = nsString.substring(with: range)
						event.set(key: fieldName, value: .text(valueString))
					}
				}
			}
		}
		return event
	}
}

public extension Source where Self.Out == Event
{
	func extract(sourceField: String = Event.rawKey,
				 fields: [String],
				 regEx: NSRegularExpression) -> FieldExtract
	{
		return FieldExtract(self, sourceField: sourceField, regEx: regEx, fieldNames: fields)
	}
}
