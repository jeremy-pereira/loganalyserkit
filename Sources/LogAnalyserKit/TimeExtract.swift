//
//  TimeExtract.swift
//
//  Created by Jeremy Pereira on 27/12/2020.
//
//  Copyright (c) Jeremy Pereira 2020
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Foundation
/// A source of events where the time has been extracted from `_raw`.
///
/// Requires a  `DateFormatter` to extract the date.
///
/// If no date is found, fall back on current date for now
///
/// - Todo:
///   - Allow dates to be from somewhere other than the start of the event
///   - fallback options for dates e.g. file modification date
public struct TimeExtract: Source
{
	private var source: AnySource<Event>
	private let dateFormat: DateFormatter
	private let contextRegEx: NSRegularExpression

	/// Intialise a filter
	/// - Parameters:
	///   - source: The source to extract the date from
	///   - format: The date format for the source
	///   - context: A regular expression that matches the part of `_raw` that
	///              contains the date
	public init<S: Source>(_ source: S, format: DateFormatter, context: NSRegularExpression)
	where S.Out == Event
	{
		self.source = AnySource<Event>.wrap(source)
		self.dateFormat = format
		self.contextRegEx = context
	}

	public mutating func next() throws -> Event?
	{
		guard var inputEvent = try source.next() else { return nil }
		guard let match = contextRegEx.firstMatchString(in: inputEvent.raw)
		else
		{
			// TODO: A better strategy for filling in missing dates
			inputEvent.set(key: Event.timeKey, value: .date(Date()))
			return inputEvent
		}

		let newDate = dateFormat.date(from: match) ?? Date()
		inputEvent.set(key: Event.timeKey, value: .date(newDate))
		return inputEvent
	}
}

public extension Source where Self.Out == Event
{

	/// Convenience function to turn a source into a `TimeExtract` for chaining
	/// - Parameters:
	///   - format: The format of the date/time stamp
	///   - context: Regular expression to apply to _raw to find the bit which
	///              is the time stamp
	/// - Returns: A time extracted source
	func timeExtract(format: DateFormatter, context: NSRegularExpression) -> TimeExtract
	{
		return TimeExtract(self, format: format, context: context)
	}
}
