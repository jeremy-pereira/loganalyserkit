//
//  LogAnalyserKit.swift
//
//  Created by Jeremy Pereira on 25/11/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox
import Statistics

/// Value for a field in an event
public enum Value
{
	/// Piece of text
	case text(String)
	/// An integer
	case integer(Int)
	/// A floating point nuber
	case float(Double)
	/// A date
	case date(Date)
	/// A list of values
	case list([Value?])


	/// Converts the value to a `StatsMappable` type
	public var asStats: StatsMappable
	{
		switch self
		{
		case .text(let string):
			return string
		case .integer(let int):
			return int
		case .float(let float):
			return float
		case .date(let date):
			return date
		case .list:
			notImplemented("Can't convert a list to stats")
		}
	}

	/// The value as a string.
	///
	/// - Text is just returned as itself.
	/// - Numbers are converted using `String(value)`
	/// - dates are converted to ISI8601 format using the GMT time zone.
	public var asString: String
	{
		switch self
		{
		case .text(let string):
			return string
		case .integer(let int):
			return String(int)
		case .float(let float):
			return String(float)
		case .date(let date):
			let formatter = ISO8601DateFormatter()
			formatter.timeZone = TimeZone(secondsFromGMT: 0)
			return formatter.string(from: date)
		case .list:
			notImplemented("Can't convert a list to stats")
		}
	}
}

extension Value: Equatable
{

}

