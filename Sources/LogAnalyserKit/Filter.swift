//
//  Filter.swift
//  
//
//  Created by Jeremy Pereira on 27/12/2020.
//
//  Copyright (c) Jeremy Pereira 2020
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

public struct Filter: Source
{
	private var source: AnySource<Event>
	private var filter: (Event) throws -> Bool

	/// Intialise a filter
	/// - Parameters:
	///   - source: The source to filter
	///   - where: Closure to filter on
	public init<S: Source>(_ source: S, where: @escaping (Event) throws -> Bool)
	where S.Out == Event
	{
		self.source = AnySource<Event>.wrap(source)
		self.filter = `where`
	}

	public mutating func next() throws -> Event?
	{
		var ret: Event? = nil
		while ret == nil, let sourceEvent = try source.next()
		{
			if try filter(sourceEvent)
			{
				ret = sourceEvent
			}
		}
		return ret
	}
}

public extension Source where Self.Out == Event
{
	func filter(where: @escaping (Event) throws -> Bool) -> Filter
	{
		return Filter(self, where: `where`)
	}
}
