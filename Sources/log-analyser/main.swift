//
//  main.swift
//  
//  Created by Jeremy Pereira on 26/12/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ArgumentParser
import LogAnalyserKit
import Statistics

extension Event: Recordable
{
	public var data: [String : StatsMappable]
	{
		var ret: [String : StatsMappable] = [:]
		for (key, value) in fields
		{
			ret[key] = value.asStats
		}
		return ret
	}
}

struct LogAnalyser: ParsableCommand
{
	@Argument(
		help: "Log files to analyse")
	var files: [String] = []
	@Option(name: .shortAndLong, help: "SQLite3 database in which to dump results")
	var statsFile: String

	func run() throws
	{
		let stats = try Statistics<Event>(sqlite3File: statsFile)
		defer { stats.finish() }

		let urls = files.map{ URL(fileURLWithPath: $0) }
		let dateFormat = DateFormatter()
		dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
		let context = try! NSRegularExpression(pattern: #"\d*-\d*-\d* \d*:\d*:\d*"#, options: [])

		let fields = ["sourceIp",  "method", "uriStem", "uriQuery", "port", "username",
					  "clientIp", "userAgent", "referrer", "status", "subStatus", "win32Status", "timeTaken"]
		let regExString = #"^[^ ]+ [^ ]+ ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+) ([^ ]+)"#

		var events = EventSplitter(FileReader(urls))
			.filter { !$0.raw.hasPrefix("#") }
			.timeExtract(format: dateFormat, context: context)
			.extract(fields: fields, regEx: try NSRegularExpression(pattern: regExString, options: []))
		var count = 0
		while let event = try events.next()
		{
			try stats.record(dataPoint: event)
			count += 1
		}
		print("Total events processed: \(count)")
	}
}

LogAnalyser.main()
